#!/usr/bin/env node

import https from 'https';
import minimist from 'minimist';
import getToken from './token_prompt.js';
import shell from 'shelljs';
import path from 'path';

const
  argv = minimist(process.argv.slice(2)),
  statusCodeAuthError = 401,
  statusCodeGood = 200,
  getHostName = (useGithub) => {
    return `api.${useGithub ? `github.com` : `bitbucket.org`}`;
  },
  getPath = (useGithub, repoOwner, repoName) => {
    return useGithub ? `/user/repos` :
        `/2.0/repositories/${repoOwner}/${repoName}`;
  },
  getRepoName = (repoName) => {
    return repoName.trim() || path.basename(process.cwd());
  },
  getUserName = (userName) => {
    return userName.trim() || shell.exec(`git config --get user.username`,
        {silent: true}).output.trim();
  },
  getRequestData = ({g, n, p}, data = {}) => {
    const isPrivate = p || false;

    if (g) {
      data.name = getRepoName(n);
      data.private = isPrivate;
    } else {
      data.scm = `git`;
      data.is_private = isPrivate;
    }
    return data;
  },
  createRequest = async function (args, opts = {}) {
    const
      repoName = getRepoName(args.n),
      userName = getUserName(args.u),
      token = args.t ? args.t : await getToken(),
      teamName = args.s ? args.s.trim() : ``,
      auth = `${userName}:${token}`.trim(),
      basic = new Buffer(auth, `ascii`).toString(`base64`);

    opts.hostname = getHostName(args.g);
    opts.path = getPath(args.g, (teamName || userName), repoName);
    opts.method = `POST`;
    opts.headers = {
      'Authorization': `BASIC ${basic}`,
      'Content-Type': `application/json`,
      'User-Agent': `NodeJS HTTPS Client (grc)`
    };

    https.request(opts, (res) => {
      if (res.statusCode === statusCodeGood) {
        console.log(`grc: repo ${repoName} created`);
      } else if (res.statusCode === statusCodeAuthError) {
        console.log(`Authorization error, check provided credentials`);
      } else {
        console.log(res.statusCode);
        res.on(`data`, (resData) => {
          console.log(`grc: Seems to be a problem with the request:`);
          console.log(`Response:\n-----------------------`);
          console.log(JSON.stringify(resData));
        });
      }
    })
      .on(`error`, (err) => {
        throw err;
      })
      .end(JSON.stringify(getRequestData(args)));
  };

if (argv.v) {
  console.log(`${require(`../package.json`).version}`);
} else {
  createRequest(argv);
}
