import prmpt from 'prompt';
import colors from 'colors/safe';

const
  tokenPrompt = (fn) => {
    prmpt.message = null;
    prmpt.delimiter = colors.rainbow(`:`);

    prmpt.start();
    prmpt.get([{
      name: `token`,
      hidden: true,
      message: colors.rainbow(`token`),
      replace: `*`
    }], fn);
  },
  promptPromise = () => {
    return new Promise((resolve, reject) => {
      tokenPrompt((err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result.token);
        }
      });
    });
  };

export default promptPromise;
